#!/usr/local/bin/python
import os

print(os.environ)

with open("pcs_iac_scan_result.xml", "w+") as f:
    f.write(
        """<?xml version="1.0" encoding="UTF-8"?>
<testsuite name="iac_scan" tests="3" failures="3">
    <testcase classname="630d3779-d932-4fbf-9cce-6e8d793c6916" 
    name="AWS S3 buckets are accessible to public" 
    file="./ui-integration-examples/public-s3.tf"
    time="0.000">
        <failure type="high" 
        message="./ui-integration-examples/public-s3.tf">
            <![CDATA[
This policy identifies S3 buckets which are publicly accessible. 
Amazon S3 allows customers to store or retrieve any type of content from anywhere in the web. Often, customers have legitimate reasons to expose the S3 bucket to public, for example, to host website content. However, these buckets often contain highly sensitive enterprise data which if left open to public may result in sensitive data leaks.

]]>
        </failure>
    </testcase>
    <testcase classname="3f141560-9cfc-412a-96cc-2768edfd23ad" 
    name="AWS S3 CloudTrail buckets for which access logging is disabled" 
    file="./ui-integration-examples/public-s3.tf"
    time="0.000">
        <failure type="low"
        message="./ui-integration-examples/public-s3.tf">
            <![CDATA[
This policy identifies S3 CloudTrail buckets for which access is disabled. 
S3 Bucket access logging generates access records for each request made to your S3 bucket. An access log record contains information such as the request type, the resources specified in the request worked, and the time and date the request was processed. It is recommended that bucket access logging be enabled on the CloudTrail S3 bucket.
]]>
        </failure>
    </testcase>
    <testcase classname="617b9138-584b-4e8e-ad15-7fbabafbed1a" 
    name="AWS Security Groups allow internet traffic to SSH port (22)" 
    file="./ui-integration-examples/0.12-security-group-blocked-ports.tf,./ui-integration-examples/0.13-security-group-blocked-ports.tf"
    time="0.000">
        <failure type="high"
        message="./ui-integration-examples/0.12-security-group-blocked-ports.tf">
            <![CDATA[
This policy identifies AWS Security Groups which do allow inbound traffic on SSH port (22) from public internet. 
Doing so, may allow a bad actor to brute force their way into the system and potentially get access to the entire network.
]]>
        </failure>
    </testcase>
</testsuite>"""
    )
